package com.example.fcmdemo;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.db.DBHelper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        // Create the list fragment and add it as our sole content.
        if (fm.findFragmentById(android.R.id.content) == null) {
            GcmMessageListFragment list = new GcmMessageListFragment();
            fm.beginTransaction().add(android.R.id.content, list).commit();
        }
    }


    public static class GcmMessageListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor> {

        SimpleCursorAdapter mAdapter;


        private static final String TAG = "GcmMessageListFragment";

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);
            setEmptyText("No Message is available");

            // Create an empty adapter we will use to display the loaded data.
            mAdapter = new SimpleCursorAdapter(getActivity(),
                    R.layout.gcm_message_row, null,
                    new String[]{DBHelper.MainTable.COLUMN_NAME_GCM_MESSAGES, DBHelper.MainTable.COLUMN_NAME_GCM_RECEIVE_DATETIME},
                    new int[]{R.id.message, R.id.dateTime}, 0);
            mAdapter.setViewBinder(new SimpleCursorAdapter.ViewBinder() {
                @Override
                public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
                    TextView tv = (TextView) view;
                    switch (tv.getId()) {
                        case R.id.dateTime:
                            int dateTimeIndex = cursor.getColumnIndex(DBHelper.MainTable.COLUMN_NAME_GCM_RECEIVE_DATETIME);
                            if (dateTimeIndex == columnIndex) {
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
                                try {
                                    String string = cursor.getString(dateTimeIndex);
                                    java.util.Date date = dateFormat.parse(string);

                                    Log.d(TAG, DateUtils.getRelativeTimeSpanString(date.getTime()) + "");
                                    ((TextView) view).setText(DateUtils.getRelativeTimeSpanString(date.getTime()));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                return true;
                            }
                    }

                    return false;
                }
            });
            setListAdapter(mAdapter);


            // Start out with a progress indicator.
            setListShown(false);

            // Prepare the loader.  Either re-connect with an existing one,
            // or start a new one.
            getLoaderManager().initLoader(0, null, this);

        }


        @Override
        public void onListItemClick(ListView l, View v, int position, long id) {
            // Insert desired behavior here.
            Log.i(TAG, "Item clicked: " + id);
        }

        // These are the rows that we will retrieve.
        static final String[] PROJECTION = new String[]{
                DBHelper.MainTable._ID,
                DBHelper.MainTable.COLUMN_NAME_GCM_MESSAGES,
                DBHelper.MainTable.COLUMN_NAME_GCM_RECEIVE_DATETIME
        };

        public Loader<Cursor> onCreateLoader(int id, Bundle args) {
            return new CursorLoader(getActivity(), DBHelper.MainTable.CONTENT_URI, PROJECTION, null, null, null);
        }

        public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
            mAdapter.swapCursor(data);
            if (isResumed()) {
                setListShown(true);
            } else {
                setListShownNoAnimation(true);
            }
        }

        public void onLoaderReset(Loader<Cursor> loader) {
            mAdapter.swapCursor(null);
        }
    }

}
