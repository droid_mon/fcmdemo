package com.example.util;

/**
 * Created by Rahul on 12/19/2015.
 */
public final class PreferencesKey {
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
}
