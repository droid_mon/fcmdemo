package com.example.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

/**
 * Created by Rahul on 12/19/2015.
 */
public class DBHelper extends SQLiteOpenHelper {
    /**
     * The authority(uri) we use to get to our  provider.
     */
    public static final String AUTHORITY = "com.example.fcmdemo.DBHelper";


    private static final String DB_NAME = "fcm_demo.db";
    private static final int DB_VERSION = 1;
    private static final String TAG = "DBHelper";

    /**
     * @param context {@link Context}
     */
    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    /**
     * {@inheritDoc}
     *
     * @param sqLiteDatabase
     */
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE " + MainTable.TABLE_NAME + " ("
                + MainTable._ID + " INTEGER PRIMARY KEY,"
                + MainTable.COLUMN_NAME_GCM_MESSAGES + " TEXT,"
                + MainTable.COLUMN_NAME_GCM_RECEIVE_DATETIME + " TIMESTAMP DEFAULT (datetime('now','localtime'))"
                + ");");
    }

    /**
     * {@inheritDoc}
     *
     * @param sqLiteDatabase
     */
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // Logs that the database is being upgraded
        Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                + newVersion + ", which will destroy all old data");

        // Kills the table and existing data
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS notes");
    }

    public static final class MainTable implements BaseColumns {
        /**
         * The table name offered by this provider
         */
        public static final String TABLE_NAME = "fcm_messages";

        /**
         * The content:// style URL for this table
         */
        public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/fcm_messages");

        /**
         * The content URI base for a single row of data. Callers must
         * append a numeric row id to this Uri to retrieve a row
         */
        public static final Uri CONTENT_ID_URI_BASE = Uri.parse("content://" + AUTHORITY + "/fcm_messages/");

        /**
         * Column name for the single column holding our gcm messages.
         * <P>Type: TEXT</P>
         */
        public static final String COLUMN_NAME_GCM_MESSAGES = "fcm_messages";

        /**
         * just to know when gcm message receive
         */
        public static final String COLUMN_NAME_GCM_RECEIVE_DATETIME = "fcm_date_time";


    }
}
