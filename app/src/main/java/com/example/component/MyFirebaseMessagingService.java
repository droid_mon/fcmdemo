package com.example.component;

import android.content.ContentValues;
import android.content.res.ObbInfo;
import android.os.Bundle;
import android.util.Log;

import com.example.db.DBHelper;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Rahul on 12/19/2015.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FBService";


    private void onMessageReceived(String from, Map<String, String> data) {
        String message =  data.get("message");
        Log.d(TAG, "Message: " + message);
        /**     - Store message in local database.*/
        ContentValues cv = new ContentValues();
        cv.put(DBHelper.MainTable.COLUMN_NAME_GCM_MESSAGES, message);
        getContentResolver().insert(DBHelper.MainTable.CONTENT_ID_URI_BASE, cv);

    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        onMessageReceived(remoteMessage.getFrom(), remoteMessage.getData());
    }
}
