package com.example.component;

import android.content.Intent;

import com.google.firebase.iid.FirebaseInstanceIdService;


/**
 * Created by Rahul on 12/19/2015.
 */
public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        // Fetch updated Instance ID token and notify our app's server of any changes (if applicable).
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);
    }
}

