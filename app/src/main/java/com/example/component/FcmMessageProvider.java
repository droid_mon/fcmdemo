package com.example.component;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v4.database.DatabaseUtilsCompat;

import com.example.db.DBHelper;

import java.util.HashMap;

/**
 * Created by Rahul on 12/19/2015.
 */
public class FcmMessageProvider extends ContentProvider {
    // A projection map used to select columns from the database
    private final HashMap<String, String> mGcmMessageProjection;
    // Uri matcher to decode incoming URIs.
    private final UriMatcher mUriMatcher;

    // The incoming URI matches the main table URI pattern
    private static final int MAIN = 1;
    // The incoming URI matches the main table row ID URI pattern
    private static final int MAIN_ID = 2;

    // Handle to a new DatabaseHelper.
    private DBHelper mDBHelper;

    public FcmMessageProvider() {
        // Create and initialize URI matcher.
        mGcmMessageProjection = new HashMap<>();
        mUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        mUriMatcher.addURI(DBHelper.AUTHORITY, DBHelper.MainTable.TABLE_NAME, MAIN);
        mUriMatcher.addURI(DBHelper.AUTHORITY, DBHelper.MainTable.TABLE_NAME + "/#", MAIN_ID);

        mGcmMessageProjection.put(DBHelper.MainTable._ID, DBHelper.MainTable._ID);
        mGcmMessageProjection.put(DBHelper.MainTable.COLUMN_NAME_GCM_MESSAGES, DBHelper.MainTable.COLUMN_NAME_GCM_MESSAGES);
        mGcmMessageProjection.put(DBHelper.MainTable.COLUMN_NAME_GCM_RECEIVE_DATETIME, DBHelper.MainTable.COLUMN_NAME_GCM_RECEIVE_DATETIME);
    }

    @Override
    public boolean onCreate() {
        mDBHelper = new DBHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(DBHelper.MainTable.TABLE_NAME);
        switch (mUriMatcher.match(uri)) {
            case MAIN:
                builder.setProjectionMap(mGcmMessageProjection);
                break;
            case MAIN_ID:
                builder.setProjectionMap(mGcmMessageProjection);
                builder.appendWhere(DBHelper.MainTable._ID + "=?");
                selectionArgs = DatabaseUtilsCompat.appendSelectionArgs(selectionArgs, new String[]{uri.getLastPathSegment()});
            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
        SQLiteDatabase db = mDBHelper.getReadableDatabase();
        Cursor c = db.query(DBHelper.MainTable.TABLE_NAME, projection, selection, selectionArgs, null /* no group */, null /* no filter */, sortOrder);
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override

    public Uri insert(Uri uri, ContentValues initialValues) {
        if (mUriMatcher.match(uri) != MAIN) {
            // Can only insert into to main URI.
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        ContentValues values;

        if (initialValues != null) {
            values = new ContentValues(initialValues);
        } else {
            values = new ContentValues();
        }

        if (values.containsKey(DBHelper.MainTable.COLUMN_NAME_GCM_MESSAGES) == false) {
            values.put(DBHelper.MainTable.COLUMN_NAME_GCM_MESSAGES, "");
        }

        SQLiteDatabase db = mDBHelper.getWritableDatabase();

        long rowId = db.insert(DBHelper.MainTable.TABLE_NAME, null, values);

        // If the insert succeeded, the row ID exists.
        if (rowId > 0) {
            Uri noteUri = ContentUris.withAppendedId(DBHelper.MainTable.CONTENT_ID_URI_BASE, rowId);
            getContext().getContentResolver().notifyChange(noteUri, null);
            return noteUri;
        }

        throw new SQLException("Failed to insert row into " + uri);
    }


    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        String finalWhere;

        int count;

        switch (mUriMatcher.match(uri)) {
            case MAIN:
                // If URI is main table, delete uses incoming where clause and args.
                count = db.delete(DBHelper.MainTable.TABLE_NAME, where, whereArgs);
                break;

            // If the incoming URI matches a single note ID, does the delete based on the
            // incoming data, but modifies the where clause to restrict it to the
            // particular note ID.
            case MAIN_ID:
                // If URI is for a particular row ID, delete is based on incoming
                // data but modified to restrict to the given ID.
                finalWhere = DatabaseUtilsCompat.concatenateWhere(
                        DBHelper.MainTable._ID + " = " + ContentUris.parseId(uri), where);
                count = db.delete(DBHelper.MainTable.TABLE_NAME, finalWhere, whereArgs);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return count;
    }

    @Override
    public int update(Uri uri, ContentValues contentValues, String where, String[] whereArgs) {
        SQLiteDatabase db = mDBHelper.getWritableDatabase();
        String finalWhere;

        int count;

        switch (mUriMatcher.match(uri)) {
            case MAIN:
                // If URI is main table, delete uses incoming where clause and args.
                count = db.update(DBHelper.MainTable.TABLE_NAME, contentValues, where, whereArgs);
                break;

            // If the incoming URI matches a single note ID, does the delete based on the
            // incoming data, but modifies the where clause to restrict it to the
            // particular note ID.
            case MAIN_ID:
                // If URI is for a particular row ID, delete is based on incoming
                // data but modified to restrict to the given ID.
                finalWhere = DatabaseUtilsCompat.concatenateWhere(
                        DBHelper.MainTable._ID + " = " + ContentUris.parseId(uri), where);
                count = db.update(DBHelper.MainTable.TABLE_NAME, contentValues, finalWhere, whereArgs);
                break;

            default:
                throw new IllegalArgumentException("Unknown URI " + uri);
        }

        getContext().getContentResolver().notifyChange(uri, null);

        return count;
    }
}
